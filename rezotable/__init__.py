#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#   Copyright (C) 2021 Christoph Fink
#
#   This program is free software; you can redistribute it and/or
#   modify it under the terms of the GNU General Public License
#   as published by the Free Software Foundation; either version 3
#   of the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, see <http://www.gnu.org/licenses/>.


"""rezotable: Sync files between a Zotero library and a ReMarkable e-reader, bidirectionally."""


__all__ = [
    "RemarkableDocumentLister",
    "ZoteroAttachmentLister",
    "ZoteroRemarkableSyncer",
    "__version__",
]


from .remarkabledocumentlister import RemarkableDocumentLister
from .zoteroattachmentlister import ZoteroAttachmentLister
from .zoteroremarkablesyncer import ZoteroRemarkableSyncer


__version__ = "0.0.1"
