#!/usr/bin/env python3

"""RemarkableDocumentLister: List all rmcl.Documents saved in a ReMarkable cloud storage."""

import datetime
import os
import os.path
import typing

import rmcl


class RemarkableDocumentLister:
    """List all rmcl.Documents saved in a ReMarkable cloud storage."""

    def __init__(self):
        """Initialise a RemarkableDocumentLister."""
        rmcl.api.get_client_s()  # make sure we have an API auth
        self._documents_by_path = {}

    @property
    def documents_by_path(self) -> dict[str, rmcl.Item]:
        """List all rmcl.Documents by their file path."""
        if not self._documents_by_path:
            paths_and_items = self._get_item_or_all_children()
            for path, item in paths_and_items:
                self._documents_by_path[path] = item
        return self._documents_by_path

    def _get_item_or_all_children(
        self, item: typing.Optional[rmcl.Item] = None, parent_path: str = ""
    ) -> list[str]:
        if item is None:
            item = rmcl.Item.get_by_id_s("")
            full_path = ""  # otherwise we get "//"
        else:
            full_path = "/".join((parent_path, item.name))
        try:
            paths_and_items = [
                self._get_item_or_all_children(child, full_path)
                for child in item.children
            ] + [[(full_path, item)]]
            # unnest list of lists of paths:
            paths_and_items = [
                path_and_item
                for sublist in paths_and_items
                for path_and_item in sublist
            ]
        except AttributeError:  # no children == not a folder
            paths_and_items = [(full_path, item)]
        return paths_and_items

    def update_document_contents(
            self,
            remarkable_document: rmcl.Document,
            contents: typing.BinaryIO,
            mtime: datetime.datetime
    ) -> None:
        """Update the contents of a rmcl.Document."""
        remarkable_document.upload_s(
            contents,
            rmcl.const.FileType.pdf
        )
        # remarkable_document._refresh_metadata_s()
        # remarkable_document._metadata = rmcl.api.get_client_s().get_metadata(remarkable_document.id)
        remarkable_document._metadata["Version"] += 1
        remarkable_document.mtime = mtime
        remarkable_document.update_metadata_s()

    def create_new_document(
            self,
            path: str,
            contents: typing.BinaryIO,
            mtime: datetime.datetime
    ) -> None:
        """Upload a new document to ReMarkable."""
        parent = self._mkdir_r(
            os.path.dirname(path)
        )
        remarkable_document = rmcl.Document.new(
            os.path.basename(path),
            parent.id
        )
        self.update_document_contents(
            remarkable_document,
            contents,
            mtime
        )
        self._documents_by_path[path] = remarkable_document

    def _mkdir_r(self, path: str) -> rmcl.Folder:
        if path == "/":
            path = ""  # root directory
        try:
            folder = self._documents_by_path[path]
        except KeyError:
            parent = self._mkdir_r(os.path.dirname(path))
            folder = rmcl.Folder.new(
                os.path.basename(path),
                parent.id
            )
            folder.update_metadata_s()
            self._documents_by_path[path] = folder
        return folder
