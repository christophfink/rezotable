# Sync files between a Zotero library and a ReMarkable e-reader, bidirectionally.

***Rezotable*** **is in early BETA status. Using it in production might be an absolutely bad idea (but it also may just work). If you encounter any issues, [please report them](https://gitlab.com/christoph.fink/rezotable/-/issues) and/or submit a merge request with a fix.**

This is a Python module to ... TODO

### Dependencies

The script is written in Python 3 and depends on the Python modules  ... TODO

### Installation

```shell
pip install rezotable
```

### Configuration

TODO

### Usage

#### Command line executable

```shell
python -m rezotable
```
